﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GameOfLifeCopy
{
    public partial class Game : System.Web.UI.Page
    {
        static private bool[,] SquareStatus;

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {

            if (SquareStatus == null)
            {
                SquareStatus = new bool[12, 12];
                for (int r = 0; r < SquareStatus.GetLength(0); r++)
                {
                    for (int col = 0; col < SquareStatus.GetLength(1); col++)
                    {
                        SquareStatus[r, col] = false;
                    }
                }
            }

        }
        #endregion

        #region Button Click Events
        protected void Button_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            string btnNumber = (btn.ID.ToString()).Remove(0, 6);
            //Ex: Button21 -> 21

            int num;
            int row = 0;
            int col = 0;

            if (int.TryParse(btnNumber, out num))
            {
                if (num < 10)
                {
                    row = 0;
                    int.TryParse(btnNumber.Remove(0, 1), out col);
                }
                else
                {
                    int.TryParse(btnNumber.Remove(1, 1), out row);
                    int.TryParse(btnNumber.Remove(0, 1), out col);
                }

                if (!SquareStatus[row + 1, col + 1])
                {
                    SquareStatus[row + 1, col + 1] = true;
                    btn.Style.Add("background-color", "green");
                }
                else
                {
                    SquareStatus[row + 1, col + 1] = false;
                    btn.Style.Add("background-color", "white");
                }
            }

        }

        protected void StartButton_Click(object sender, EventArgs e)
        {
            Timer2.Enabled = true;
            StartButton.Enabled = false;
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            RefreshView();
        }

        protected void StopButton_Click(object sender, EventArgs e)
        {
            Timer2.Enabled = false;
            StartButton.Enabled = true;
        }
        #endregion

        #region RefreshView + Style
        protected void RefreshView()
        {
            //Create temporary array
            bool[,] tempStatus = new bool[12, 12];
            
            for (int row = 0; row < tempStatus.GetLength(0); row++)
            {
                for (int col = 0; col < tempStatus.GetLength(1); col++)
                {
                    tempStatus[row, col] = false;
                }
            }

            for (int row = 1; row <= SquareStatus.GetLength(0) - 2; row++)
            {
                for (int col = 1; col <= SquareStatus.GetLength(1) - 2; col++)
                {

                    int alive = 0;
                    //top left
                    if (SquareStatus[row - 1, col - 1]) { alive++; }

                    //top
                    if (SquareStatus[row - 1, col]) { alive++; }

                    //top right
                    if (SquareStatus[row - 1, col + 1]) { alive++; }
                        
                    //left
                    if (SquareStatus[row, col - 1]) { alive++; }

                    //right
                    if (SquareStatus[row, col + 1]) { alive++; }

                    //bottom left
                    if (SquareStatus[row + 1, col - 1]) { alive++; }

                    //bottom
                    if (SquareStatus[row + 1, col]) { alive++; }

                    //bottom right
                    if (SquareStatus[row + 1, col + 1]) { alive++; }

                    //Game Rules
                    //If cell is already alive, it stay alive if it has 2 or 3 neighbors
                    //If the cell is dead, it will only come alive if it has 3 neighbors
                    if (SquareStatus[row, col])
                    {
                        if (alive == 2 || alive == 3)
                        {
                            tempStatus[row, col] = true;
                        }
                        else
                        {
                            tempStatus[row, col] = false;
                        }
                    }
                    else
                    {
                        if (alive == 3)
                        {
                            tempStatus[row, col] = true;
                        }    
                        else
                        {
                            tempStatus[row, col] = false;
                        }

                    }

                }

            }

            //Assigns temporary array back into the used array
            for (int row = 1; row < SquareStatus.GetLength(0) - 1; row++)
            {
                for (int col = 1; col < SquareStatus.GetLength(1) - 1; col++)
                {
                    SquareStatus[row, col] = tempStatus[row, col];
                    StyleButton(row, col);
                }
            }
            tempStatus = null;
        }

        public void StyleButton(int row, int col)
        {
            string ButtonID;

            if (row == 1)
            {
                ButtonID = "Button" + (col - 1);
            }
            else
            {
                ButtonID = "Button" + (row - 1) + (col - 1);
            }

            Button btn = FindControl(ButtonID) as Button;

            if (btn != null)
            {
                if (SquareStatus[row, col])
                {
                    btn.Style.Add("background-color", "green");
                }
                else
                {
                    btn.Style.Add("background-color", "white");
                }
            }

        }
        #endregion

        #region Timer
        protected void Timer2_Tick(object sender, EventArgs e)
        {
            RefreshView();
        }
        #endregion
    }
}
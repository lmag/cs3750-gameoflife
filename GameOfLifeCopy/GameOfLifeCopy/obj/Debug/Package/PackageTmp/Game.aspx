﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Game.aspx.cs" Inherits="GameOfLifeCopy.Game" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .button {
            background-color: white;
            margin: 0px;
            padding: 0px;
        }

        .buttongreen {
            margin: 0px;
            padding: 0px;
        }

        .buttongreen:hover {
            background-color: red;
            margin: 0px;
            padding: 0px;
        }

        .button:hover {
            background-color: red;
            margin: 0px;
            padding: 0px;
        }
    </style>
</head>
<body>
  <form id="form1" runat="server">
      <asp:UpdatePanel ID="Buttons" runat="server">
          <ContentTemplate>
        <asp:ScriptManager runat="server" id="ScriptManager1"/>
        <asp:Timer ID="Timer2" runat="server" Interval="1500" Enabled="false" OnTick="Timer2_Tick"></asp:Timer>
      
        <div/ style="align-content:center">
            <asp:Button ID="Button0" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button1" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button2" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button3" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button4" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button5" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button6" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button7" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button8" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <asp:Button ID="Button9" runat="server" CssClass="button" Width="50" Height="50"  OnClick="Button_Click"/>
            <br />                                                                           
            <asp:Button ID="Button10" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button11" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button12" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button13" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button14" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button15" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button16" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button17" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button18" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button19" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button20" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button21" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button22" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button23" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button24" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button25" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button26" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button27" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button28" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button29" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button30" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button31" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button32" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button33" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button34" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button35" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button36" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button37" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button38" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button39" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button40" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button41" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button42" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button43" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button44" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button45" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button46" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button47" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button48" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button49" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button50" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button51" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button52" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button53" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button54" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button55" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button56" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button57" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button58" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button59" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button60" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button61" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button62" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button63" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button64" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button65" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button66" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button67" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button68" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button69" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button70" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button71" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button72" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button73" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button74" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button75" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button76" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button77" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button78" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button79" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button80" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button81" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button82" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button83" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button84" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button85" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button86" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button87" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button88" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button89" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />                                                                            
            <asp:Button ID="Button90" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button91" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button92" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button93" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button94" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button95" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button96" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button97" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button98" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <asp:Button ID="Button99" runat="server" CssClass="button" Width="50" Height="50" OnClick="Button_Click" />
            <br />
            <asp:Button ID="StartButton"    runat="server" Text="Start" OnClick="StartButton_Click" />
            <asp:Button ID="NextButton"     runat="server" Text="Next"  OnClick="NextButton_Click" />
            <asp:Button ID="Stop"           runat="server" Text="Stop"  OnClick="StopButton_Click" />
            </div/</ContentTemplate>
          <Triggers>
              <asp:AsyncPostBackTrigger controlID="NextButton" EventName="Click"/>
          </Triggers>
            </asp:UpdatePanel>

        <div>
            

        </div>       
    </form>

</body>
</html>
